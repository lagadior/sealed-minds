﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PostProcessing;

public class PostProcessingController : MonoBehaviour {

    public PostProcessingBehaviour parisPostProcessing;

    public void ParisCamera()
    {
        parisPostProcessing = this.GetComponent<PostProcessingBehaviour>();
        parisPostProcessing.enabled = true;
    }

}
