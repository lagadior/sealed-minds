﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class MoveToGameObject : MonoBehaviour {

    public bool move = false;
    public GameObject target;

	void Update ( ) {

        if ( move == false ) return;

        NavMeshAgent navMeshAgent;
        navMeshAgent = gameObject.GetComponent<NavMeshAgent>( );
        navMeshAgent.enabled = true;
        navMeshAgent.SetDestination ( target.transform.position );

	}

    public void Move ( ) {
        move = true;
    }

}
