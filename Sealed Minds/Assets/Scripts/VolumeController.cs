﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VolumeController : MonoBehaviour {

    private AudioSource audioSrc;
    private float VolumeControl = 1f;


    void Start()
    {
        audioSrc = GetComponent<AudioSource>();
    }


    // Update is called once per frame
    void Update () {
        audioSrc.volume = VolumeControl;
	}

    public void SetVolume(float vol)
    {
        VolumeControl = vol;
    }
}
