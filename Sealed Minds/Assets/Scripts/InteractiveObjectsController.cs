﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;

public class InteractiveObjectsController : MonoBehaviour
{
    public string requirement;
    public Flowchart objectsFlowchart;
    public bool isVisible;

   
    void OnBecameVisible()
    {
        isVisible = true;
    }
    void OnBecameInvisible()
    {
        isVisible = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Execute ( )
    {
        //clone = Instantiate(objectsFlowchart);
        objectsFlowchart.ExecuteBlock(gameObject.name);
        
    }

    public void End ( )
    {
        Control.main.Activate();
    }

}