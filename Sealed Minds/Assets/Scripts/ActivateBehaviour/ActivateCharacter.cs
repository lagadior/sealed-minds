﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateCharacter : ActivateBehaviour {

    bool talking = false;

    public override void ActivateEnter ( Collider infoAccess ) {

        //Aqui se detecta si es Hector
        if (infoAccess.tag != "Character") return;

        Debug.Log(infoAccess.gameObject.name);

       
        scriptD01 = infoAccess.gameObject.GetComponentInChildren<DialogsController>();
        if (scriptD01 == null) Debug.LogError ( "No se pudo acceder al script DialogsController" );

        Debug.Log("Talking: " + talking);
    }

    public override void ActivateStay(Collider infoAccess)
    {

        DialogsController interactiveChac = infoAccess.GetComponent<DialogsController>();

        //Se activa el D01 y se desactiva control del jugador
        if (Input.GetMouseButton(0) && infoAccess.tag == "Character" && talking == false)
        {
            
            chaController.enabled = false;
            camController.enabled = false;
            interactiveChac.Execute();
            Cursor.lockState = CursorLockMode.None;
            scriptD01.enabled = true;
            talking = true;

        }

        if (Input.GetMouseButton(0) && infoAccess.tag == "Mirror" && talking == false)
        {

            chaController.enabled = false;
            camController.enabled = false;
            interactiveChac.Execute();
            Cursor.lockState = CursorLockMode.None;
            scriptD01.enabled = true;
            talking = true;

        }

    }

    public override void ActivateExit(Collider infoAccess)
    {
        
        if (infoAccess.tag != "Character" && infoAccess.tag != "Mirror") return;

        Debug.Log(infoAccess.gameObject.name);

        scriptD01 = infoAccess.gameObject.GetComponentInChildren<DialogsController>();
  
        if (scriptD01 == null) return;

        scriptD01.enabled = false;
        talking = false;
    }

}
