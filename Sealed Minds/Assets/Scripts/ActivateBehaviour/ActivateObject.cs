﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;

public class ActivateObject : ActivateBehaviour
{
    bool talking = false;


    public override void ActivateEnter(Collider infoAccess)
    {
        //Aqui se detecta si es un Objeto
         if (infoAccess.tag != "Object") return;

         Debug.Log(infoAccess.gameObject.name);

         if (infoAccess.tag == "Object")
         {
             scriptObj = infoAccess.gameObject.GetComponent<InteractiveObjectsController>();
             if (scriptObj == null) Debug.LogError("No se pudo acceder al script de ObjetosController");
        }
    }

    public override void ActivateStay(Collider infoAccess)
    {
        if ( infoAccess.tag != "Object") return;

        InteractiveObjectsController interactive = infoAccess.GetComponent<InteractiveObjectsController>( );
        if (interactive.requirement != "")
        {
            if (interactive.objectsFlowchart.GetBooleanVariable(interactive.requirement) == true)
            {
                if (Input.GetMouseButtonDown(0) && talking == false && interactive.isVisible )
                {

                    chaController.enabled = false;
                    camController.enabled = false;
                    Cursor.lockState = CursorLockMode.None;
                    interactive.Execute();
                    Debug.Log("Desactivando controles de jugador para iniciar diálogo");
                    talking = true;
                }


            }
        }
        //Se activa el Obj y se desactiva control del jugador
        if (Input.GetMouseButtonDown(0) && infoAccess.tag == "Object" && talking == false && interactive.requirement == "" && interactive.isVisible )
        {
            
            
                chaController.enabled = false;
                camController.enabled = false;
                Cursor.lockState = CursorLockMode.None;
                interactive.Execute();
                Debug.Log("Desactivando controles de jugador para iniciar diálogo");
                talking = true;
            
        }
        
    }

    public override void ActivateExit(Collider infoAccess)
    {
        if (infoAccess.tag != "Object") return;

        Debug.Log(infoAccess.gameObject.name);

        scriptObj = infoAccess.gameObject.GetComponent<InteractiveObjectsController>();

        talking = false;

        //if (scriptObj == null) return;

        //scriptObj.enabled = false;
    }

}
