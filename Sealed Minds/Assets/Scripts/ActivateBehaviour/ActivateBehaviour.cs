﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;

public class ActivateBehaviour : MonoBehaviour {

    public DialogsController scriptD01;
    public InteractiveObjectsController scriptObj;

    public CharacterController chaController;
    public CamaraController camController;

   

    // Update is called once per frame
    void OnTriggerEnter ( Collider infoAccess ) {

        // Acceso a cámara y controlador para detenerlo durante el diálogo
        chaController = this.GetComponent<CharacterController>();
        camController = gameObject.GetComponentInChildren<CamaraController>();

        ActivateEnter ( infoAccess );    

	}

    private void OnTriggerStay(Collider infoAccess)
    {

        ActivateStay(infoAccess);


            
    }

    void OnTriggerExit(Collider infoAccess)
    {


        ActivateExit(infoAccess);


    }


    //Recuperar el control del personaje
    public void RecoverCharacterControl()
    {
        chaController.enabled = true;
        camController.enabled = true;
        Cursor.lockState = CursorLockMode.Locked;
    }






    public virtual void ActivateEnter ( Collider info )
    {

    }

    public virtual void ActivateStay(Collider info)
    {

    }

    public virtual void ActivateExit(Collider info)
    {

    }

}
