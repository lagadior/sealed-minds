﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicController : MonoBehaviour
{

    private AudioSource audioSrc;
    private float MusicControl = 1f;


    void Start()
    {
        audioSrc = GetComponent<AudioSource>();
    }


    // Update is called once per frame
    void Update()
    {
        audioSrc.volume = MusicControl;
    }

    public void SetVolume(float vol)
    {
        MusicControl = vol;
    }
}