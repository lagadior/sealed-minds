﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioControl : MonoBehaviour {

    public static AudioControl main;
    private void Awake() {
        if ( main == null ) main = this;
    }

    public void FadeVolume ( float volume ) {
        StartCoroutine ( FadeVolumeCoroutine ( volume ) );
	}

    IEnumerator FadeVolumeCoroutine ( float volume ) {
        float timer = 0f;
        while ( timer <= 1f )  {
            AudioListener.volume = Mathf.Lerp( AudioListener.volume, volume , timer );
            timer += Time.deltaTime;
            Debug.Log("Audio Listener Volume " + AudioListener.volume);
            yield return null;
        }
    }

}
