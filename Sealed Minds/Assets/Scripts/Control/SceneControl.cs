﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneControl : MonoBehaviour {

    private int indexToLoad;

    public static SceneControl main;
    private void Awake()
    {
        if (main == null) main = this;
    }

    public void LoadScene ( int index ) {
        indexToLoad = index;
        AudioControl.main.FadeVolume ( 0f );
        Invoke ( "Change", 1f );
	}

    // Se debería cambiar por una corrutina de carga de escena asíncrona
    public void Change ( ) {
        SceneManager.LoadScene ( indexToLoad );
    }


}
