﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class CinematicEnd : MonoBehaviour {

    long playerCurrentFrame;
    long playerFrameCount;
    private ActivateBehaviour aB;



    // Use this for initialization
    void Start () {

        aB = GameObject.Find("Helena").GetComponent<ActivateBehaviour>();
	}
	
	// Update is called once per frame
	void Update () {

        playerCurrentFrame = gameObject.GetComponent<VideoPlayer>().frame +1;
        playerFrameCount = Convert.ToInt64(gameObject.GetComponent<VideoPlayer>().frameCount);

        VideoEnd();

    }

    public void VideoEnd()
    {
        if (playerCurrentFrame == playerFrameCount) {
            aB.RecoverCharacterControl();
            Destroy(gameObject);
        }
        
    }

}  
