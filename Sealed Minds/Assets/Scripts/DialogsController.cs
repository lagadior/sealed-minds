﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;

public class DialogsController : MonoBehaviour
{
    public string requirement;
    public Flowchart hectorFlowchart;
    public bool isVisible;


    void OnBecameVisible()
    {
        isVisible = true;
    }
    void OnBecameInvisible()
    {
        isVisible = false;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Execute()
    {
        //clone = Instantiate(objectsFlowchart);
        hectorFlowchart.ExecuteBlock("Hector");

    }

    public void End()
    {
        Control.main.Activate();
    }

}