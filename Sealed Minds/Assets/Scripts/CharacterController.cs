﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterController : MonoBehaviour {

    public float speed = 2.0f;
    public Rigidbody rg;
   

	// Use this for initialization
	void Start () {

        Cursor.lockState = CursorLockMode.Locked;

	}
	
	// Update is called once per frame
	void Update () {

        //float translation = Input.GetAxis("Vertical") * speed;
        //float straffe = Input.GetAxis("Horizontal") * speed;
        //translation *= Time.deltaTime;
        //straffe *= Time.deltaTime;

        //transform.Translate(straffe, 0, translation);
        float speedY = rg.velocity.y;
        rg.velocity = transform.forward * Input.GetAxis("Vertical") * speed;
        rg.velocity += transform.right * Input.GetAxis("Horizontal") * speed / 2f;
        rg.velocity = new Vector3(rg.velocity.x, speedY, rg.velocity.z);

        if (Input.GetKey(KeyCode.W))
        {
            GetComponent<Animator>().SetBool("Walking", true);
        }
        else
        {
            GetComponent<Animator>().SetBool("Walking", false);
        }

        //if (Input.GetMouseButton(0))
        //{
        //    Cursor.lockState = CursorLockMode.None;
        //    speed = 0;
        //}
        //else
        //{

        //    speed = 3f;
        //}


    }

}
